import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
  styleUrls: ['navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    public queryMoviesService: MoviesService,
    private router: Router
  ) { }
  ngOnInit() {}

  onSubmit(form: any): void {
    console.log('you submitted value:', form);
    this.queryMoviesService.getMovieRequest(form.searchInput);
    this.router.navigate(['/home']);
  }
}
