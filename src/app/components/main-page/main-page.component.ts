import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import Movie from '../../classes/movie';
import {Router} from "@angular/router";

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css','../app.component.css']
})
export class MainPageComponent implements OnInit {

  movies: Movie[] = [];

  constructor(
    public queryMoviesService: MoviesService,
    private router: Router
  ) {
    this.queryMoviesService.moviesChange.subscribe((value: Movie[]) => {
      this.movies = value;
    });
  }

  ngOnInit() { }

  onSubmit(form: any): void {
    console.log('you submitted value:', form);
    this.queryMoviesService.getMovieRequest(form.searchInput);
  }

  onMoreInfo(movieID: number) :void{
    const linkTo = ['/details', movieID];
    this.router.navigate(linkTo);
  }

}
