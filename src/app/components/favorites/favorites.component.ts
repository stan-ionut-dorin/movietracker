import { Component, OnInit } from '@angular/core';
import Movie from "../../classes/movie";
import {MoviesService} from "../../services/movies.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-favorites',
  templateUrl: 'favorites.component.html',
  styleUrls: ['favorites.component.css', '../app.component.css']
})
export class FavoritesComponent implements OnInit {

  favoriteMovies: Movie[];

  constructor(
    private service: MoviesService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.favoriteMovies = this.service.favoriteMovies;
  }

  onMoreInfo(movieID: number) :void{
    const linkTo = ['/details', movieID];
    this.router.navigate(linkTo);
  }

}
