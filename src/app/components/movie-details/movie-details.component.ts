import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import {MoviesService} from "../../services/movies.service";
import Movie from "../../classes/movie";
import {MOVIES} from "../../mocks/mock-movies";
import {isNullOrUndefined} from "util";
import {isUndefined} from "util";

@Component({
  selector: 'app-movie-details',
  templateUrl: 'movie-details.component.html',
  styleUrls: ['movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {

  movieDetails: {} = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: MoviesService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['id'];
      console.log('Bla bla id', id);
      this.service.getMovieDetailsRequest(id);
      this.service.moviesDetailsChange.subscribe((value: {}) => {
        this.movieDetails = value;
        console.log('This is the movie: ', this.movieDetails);
      });
    });
  }

  onAddFavorite(id: number) {
    this.service.addFavoriteMovie(id);
  }
}
