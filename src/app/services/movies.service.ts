import { Injectable } from '@angular/core';
import {Subject} from "rxjs";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config, QueryMovie } from '../interfaces/interfaces';
import apiConfig from '../config/tmdbConfig';

import Movie from '../classes/movie';
import { MOVIES } from '../mocks/mock-movies';

@Injectable()
export class MoviesService {

  movies: Movie[] = MOVIES;
  moviesChange: Subject<Movie[]> = new Subject<Movie[]>();
  movieDetails: {} = {};
  moviesDetailsChange: Subject<{}> = new Subject<{}>();
  configImages: {} = {};
  favoriteMovies: Movie[] = [];

  constructor(private http: HttpClient) { }

  addFavoriteMovie(id: number) :void{
    const wasAdded = this.favoriteMovies.find((movie)=>(movie.id === id));
    if (wasAdded === undefined)
      this.favoriteMovies.push(this.movies.find((movie)=>(movie.id === id)));
    console.log('Favortie movies: ',this.favoriteMovies);
  }

  setMovies(movies: Movie[]) {
    this.movies = movies;
    this.moviesChange.next(this.movies);
  }

  getConfigRequest() {
    let requestParams = new HttpParams();
    requestParams = requestParams.set('api_key', apiConfig.api_key);
    const options = {
      params: requestParams
    };
    let request = this.http.get<Config>(apiConfig.configUrl, options);
    request.subscribe(data => {
      this.configImages = data.images;
    });
  }

  getMovieRequest(query: string) {
    let requestParams = new HttpParams();
    requestParams = requestParams.set('api_key', apiConfig.api_key);
    requestParams = requestParams.set('language', 'en-US');
    requestParams = requestParams.set('include_adult', 'false');
    requestParams = requestParams.set('page', '1');
    requestParams = requestParams.set('query', query);
    console.warn(requestParams);
    const options = {
      params: requestParams
    };
    let request = this.http.get<QueryMovie>(apiConfig.searchUrl, options);
    request.subscribe(data => {
      console.warn('query', data);
      this.setMovies(data.results.map((item) => {
        item['poster_path'] = this.configImages['secure_base_url'] + this.configImages['poster_sizes'][3] + item['poster_path'];
        item['backdrop_path'] = this.configImages['secure_base_url'] + this.configImages['backdrop_sizes'][1] + item['backdrop_path'];
        return item;
      }));
    });
  }

  getMovieDetailsRequest(movieID: number) {
    let requestParams = new HttpParams();
    requestParams = requestParams.set('api_key', apiConfig.api_key);
    requestParams = requestParams.set('language', 'en-US');
    requestParams = requestParams.set('query', movieID.toString());
    console.warn(requestParams);
    const options = {
      params: requestParams
    };
    let request = this.http.get<{}>(apiConfig.detailsUrl+movieID, options);
    request.subscribe(data => {
      console.warn('Request details for movie', data);
      data['poster_path'] = this.configImages['secure_base_url'] + this.configImages['poster_sizes'][3] + data['poster_path'];
      data['backdrop_path'] = this.configImages['secure_base_url'] + this.configImages['backdrop_sizes'][1] + data['backdrop_path'];
      data['genres']=data['genres'].map((genre) => (genre.name));
      this.movieDetails = data;
      this.moviesDetailsChange.next(this.movieDetails);
      });
    };
}
