import Movie from '../classes/movie';

export const MOVIES: Movie[] = [
  {
    "vote_count": 4785,
    "id": 374720,
    "video": false,
    "vote_average": 7.4,
    "title": "Dunkirk",
    "popularity": 124.639723,
    "poster_path": "/ebSnODDg9lbsMIaWg2uAbjn7TO5.jpg",
    "original_language": "en",
    "original_title": "Dunkirk",
    "genre_ids": [
      28,
      18,
      36,
      53,
      10752
    ],
    "backdrop_path": "/4yjJNAgXBmzxpS6sogj4ftwd270.jpg",
    "adult": false,
    "overview": "The story of the miraculous evacuation of Allied soldiers from Belgium, Britain, Canada and France, who were cut off and surrounded by the German army from the beaches and harbour of Dunkirk between May 26th and June 4th 1940 during World War II.",
    "release_date": "2017-07-19"
  },
  {
    "vote_count": 7022,
    "id": 329865,
    "video": false,
    "vote_average": 7.3,
    "title": "Arrival",
    "popularity": 29.036651,
    "poster_path": "/hLudzvGfpi6JlwUnsNhXwKKg4j.jpg",
    "original_language": "en",
    "original_title": "Arrival",
    "genre_ids": [
      53,
      18,
      878,
      9648
    ],
    "backdrop_path": "/yIZ1xendyqKvY3FGeeUYUd5X9Mm.jpg",
    "adult": false,
    "overview": "Taking place after alien crafts land around the world, an expert linguist is recruited by the military to determine whether they come in peace or are a threat.",
    "release_date": "2016-11-10"
  },
  {
    "vote_count": 4250,
    "id": 324786,
    "video": false,
    "vote_average": 7.9,
    "title": "Hacksaw Ridge",
    "popularity": 37.609826,
    "poster_path": "/bndiUFfJxNd2fYx8XO610L9a07m.jpg",
    "original_language": "en",
    "original_title": "Hacksaw Ridge",
    "genre_ids": [
      18,
      36,
      10752
    ],
    "backdrop_path": "/zBK4QZONMQXhcgaJv1YYTdCW7q9.jpg",
    "adult": false,
    "overview": "WWII American Army Medic Desmond T. Doss, who served during the Battle of Okinawa, refuses to kill people and becomes the first Conscientious Objector in American history to receive the Congressional Medal of Honor.",
    "release_date": "2016-10-07"
  }
];
