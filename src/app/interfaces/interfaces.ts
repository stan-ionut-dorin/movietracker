import Movie from "../classes/movie";

export interface Config {
  change_keys: any[];
  images: {};
}

export interface QueryMovie {
  page: number;
  results: Movie[];
  total_pages: number;
  total_results: number;
}
