// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCYyifrOu8B1KEa0pyPRD2wFhwB25QXlqA",
    authDomain: "movie-tracker-ng5.firebaseapp.com",
    databaseURL: "https://movie-tracker-ng5.firebaseio.com",
    projectId: "movie-tracker-ng5",
    storageBucket: "",
    messagingSenderId: "1071995176785"
  }
};
